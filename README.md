# LiCS HCSe MgHSe Ternary Systems at 150GPa

This repository contains the structures in the phase diagrams of the Li-C-S, H-C-Se and Mg-H-Se ternary systems presented in the "Phase diagrams of high-pressure ternary systems" thesis of João N.Q. Oliveira

All structures files are POSCAR files used in the Vienna Ab initio Simulation Package (Vasp code).
