# Structures

The folders contain the POSCAR files from the compounds in the phase diagrams, both all of our own structures and those from Tables 5.1 and 5.2 after the calculations to be compatible.

## Description of the folders names:

### The folder "for_phase_diagram" contains the structures from Tables 5.1 and 5.2 after re-optimized at 150GPa using the same parameters we use for our calculations.
| abreviation| definition |
|-|-|
|nRef | reference to the number of the compounds in Tables 5.1 and 5.2|
|spgrp | space group from our calculations|
|Composition | the element or the reduced unit cell|

We have the folderes named as {nRef}_{spgrp}-{Composition}, for example: `n7_15-H` or `n28_12-HSe2`

### The folders that have the ternary systems' names: HCSe, LiCS and MgHSe, contain the compounds obtains in each ternary systems search. 
Within the folders we have the following:

| abreviation| definition |
|-|-|
|spgrp | space group from our calculations|
|Composition | the reduced unit cell|

We have the folderes named as {spgrp}-{Composition}, for example: `8-H3CSe2`, `10-Li5CS` or `164-Mg2H3Se`
